import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Shopping cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_company'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/dropdown_select_country'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/dropdown_state_province'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_address_1'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_address_2'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_zip_postal_code'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_phone_number'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/input_fax_number'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/btn_billing_address_continue'))

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_company'), company)

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Checkout/Billing address/dropdown_select_country'), country, false)

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Checkout/Billing address/dropdown_state_province'), state_province, 
    false)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_city'), city)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_address_1'), address_1)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_address_2'), address_2)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_zip_postal_code'), zip_postal_code)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_phone_number'), phone_number)

WebUI.setText(findTestObject('ObjectSpy/Checkout/Billing address/input_fax_number'), fax_number)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Billing address/btn_billing_address_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Billing address/btn_billing_address_continue'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Shipping address/btn_shipping_address_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Shipping address/btn_shipping_address_continue'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Shipping method/radio_shipping_method_next_day_air'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Shipping method/radio_shipping_method_next_day_air'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Shipping method/btn_shipping_method_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Shipping method/btn_shipping_method_continue'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Payment method/radio_payment_method_cod'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Payment method/radio_payment_method_cod'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Payment method/btn_payment_method_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Payment method/btn_payment_method_continue'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Payment information/lbl_payment_information'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Payment information/btn_payment_information_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Payment information/btn_payment_information_continue'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Confirm order/tbl_confirm_order'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Checkout/Confirm order/btn_confirm_order_continue'))

WebUI.click(findTestObject('ObjectSpy/Checkout/Confirm order/btn_confirm_order_continue'))

WebUI.callTestCase(findTestCase('Order success'), [:], FailureHandling.STOP_ON_FAILURE)

