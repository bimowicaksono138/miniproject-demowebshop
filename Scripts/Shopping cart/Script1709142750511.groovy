import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Add product to shopping cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('ObjectSpy/Shopping cart/link_shopping_cart'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/input_product_qty'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/btn_update_cart'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/dropdown_country'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/dropdown_state_province'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/input_zip_postal_code'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/checkbox_terms'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Shopping cart/btn_checkout'))

WebUI.setText(findTestObject('ObjectSpy/Shopping cart/input_product_qty'), product_qty)

WebUI.click(findTestObject('ObjectSpy/Shopping cart/btn_update_cart'))

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Shopping cart/dropdown_country'), country, false)

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Shopping cart/dropdown_state_province'), state_province, false)

WebUI.setText(findTestObject('ObjectSpy/Shopping cart/input_zip_postal_code'), zip_postal_code)

WebUI.click(findTestObject('ObjectSpy/Shopping cart/btn_estimate_shipping'))

WebUI.verifyElementText(findTestObject('ObjectSpy/Shopping cart/lbl_ground'), 'Ground (0.00)')

WebUI.verifyElementText(findTestObject('ObjectSpy/Shopping cart/lbl_next_day'), 'Next Day Air (0.00)')

WebUI.verifyElementText(findTestObject('ObjectSpy/Shopping cart/lbl_2nd_day'), '2nd Day Air (0.00)')

WebUI.verifyElementText(findTestObject('ObjectSpy/Shopping cart/lbl_in_store_pickup'), 'In-Store Pickup (0.00)')

WebUI.click(findTestObject('ObjectSpy/Shopping cart/checkbox_terms'))

WebUI.click(findTestObject('ObjectSpy/Shopping cart/btn_checkout'))

