import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Order success/lbl_order_has_been_successfully_processed'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Order success/lbl_order_information'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Order success/lbl_order_number'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Order success/link_order_details'))

WebUI.click(findTestObject('ObjectSpy/Order success/link_order_details'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Order success/tbl_order_information'))

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Review product'), [('review_title') : 'the product is good', ('review_text') : 'good product just like image'], 
    FailureHandling.STOP_ON_FAILURE)

