import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/link_to_home'))

WebUI.click(findTestObject('ObjectSpy/Review product/link_to_home'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Add product to shopping cart/link_laptop'))

WebUI.click(findTestObject('ObjectSpy/Add product to shopping cart/link_laptop'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/link_add_review'))

WebUI.click(findTestObject('ObjectSpy/Review product/link_add_review'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/input_review_title'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/textarea_rating_text'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/btn_submit_review'))

WebUI.setText(findTestObject('ObjectSpy/Review product/input_review_title'), review_title)

WebUI.setText(findTestObject('ObjectSpy/Review product/textarea_rating_text'), review_text)

WebUI.click(findTestObject('ObjectSpy/Review product/btn_submit_review'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Review product/lbl_product_review_success'))

WebUI.delay(5)

WebUI.closeBrowser()

