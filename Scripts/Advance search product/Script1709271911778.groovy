import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Search product'), [('search_keyword') : 'computing'], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/input_search_keyword'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/checkbox_advance_search'))

WebUI.setText(findTestObject('ObjectSpy/Search product/Advance search/input_search_keyword'), search_keyword)

WebUI.click(findTestObject('ObjectSpy/Search product/Advance search/checkbox_advance_search'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/dropdown_category'))

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Search product/Advance search/dropdown_category'), category, false)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/checkbox_sub_category'))

WebUI.click(findTestObject('ObjectSpy/Search product/Advance search/checkbox_sub_category'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/dropdown_manufacturer'))

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Search product/Advance search/dropdown_manufacturer'), manufacturer, 
    false)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/checkbox_search_in_product_description'))

WebUI.click(findTestObject('ObjectSpy/Search product/Advance search/checkbox_search_in_product_description'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/input_price_from'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/input_price_to'))

WebUI.setText(findTestObject('ObjectSpy/Search product/Advance search/input_price_from'), price_from)

WebUI.setText(findTestObject('ObjectSpy/Search product/Advance search/input_price_to'), price_to)

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/dropdown_display_perpage'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/dropdown_sortby'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Search product/Advance search/dropdown_view_as'))

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Search product/Advance search/dropdown_display_perpage'), display_perpage, 
    false)

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Search product/Advance search/dropdown_sortby'), sort_by, false)

WebUI.selectOptionByLabel(findTestObject('ObjectSpy/Search product/Advance search/dropdown_view_as'), view_as, false)

WebUI.delay(5)

WebUI.closeBrowser()

