import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.verifyElementVisible(findTestObject('ObjectRecord/Login/link_login'))

WebUI.click(findTestObject('ObjectRecord/Login/link_login'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/btn_register'))

WebUI.click(findTestObject('ObjectSpy/Register/btn_register'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/radio_gender_male'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/input_first_name'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/input_last_name'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/input_email'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/input_password'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/input_confirm_password'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/btn_register_account'))

WebUI.click(findTestObject('ObjectSpy/Register/radio_gender_male'))

WebUI.setText(findTestObject('ObjectSpy/Register/input_first_name'), firstname)

WebUI.setText(findTestObject('ObjectSpy/Register/input_last_name'), lastname)

WebUI.setText(findTestObject('ObjectSpy/Register/input_email'), email)

WebUI.setText(findTestObject('ObjectSpy/Register/input_password'), password)

WebUI.setText(findTestObject('ObjectSpy/Register/input_confirm_password'), confirmpassword)

WebUI.click(findTestObject('ObjectSpy/Register/btn_register_account'))

WebUI.verifyElementVisible(findTestObject('ObjectSpy/Register/btn_register_complete'))

WebUI.click(findTestObject('ObjectSpy/Register/btn_register_complete'))

WebUI.delay(5)

WebUI.closeBrowser()

