<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email</name>
   <tag></tag>
   <elementGuidId>18514f6b-f700-4f71-9802-ba44a811ffd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>52ca1a9d-153b-4cf2-9676-60daa5f5d24f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>1d5bf29e-b624-4974-8147-58699053ca7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>221acb4f-e684-4402-afb8-6848eb312947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>567c2d73-9b67-49d0-9bbc-751aad6798bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>b0b4c4b3-b2a0-4136-b3fa-2b662d577dcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>d056ca5d-33c1-41ab-a0a9-630b22ed6666</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Email&quot;)</value>
      <webElementGuid>caee51e2-bf83-4289-b260-120ccfa17cab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Email']</value>
      <webElementGuid>338a2c93-e5ba-42a9-9747-dd09fa324516</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/input</value>
      <webElementGuid>579abe34-3129-4f51-9e55-42d44e0a5050</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Email' and @name = 'Email' and @type = 'text']</value>
      <webElementGuid>c03346e7-0743-4d66-8d21-f71f46c1d4ba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
