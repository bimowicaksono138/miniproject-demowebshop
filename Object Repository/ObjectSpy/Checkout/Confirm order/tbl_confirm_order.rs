<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tbl_confirm_order</name>
   <tag></tag>
   <elementGuidId>a2f6549a-869b-4be7-a783-ccc02193e48b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-step-confirm-order']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#checkout-step-confirm-order</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3076184f-337b-466d-ba55-180df191d4f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkout-step-confirm-order</value>
      <webElementGuid>b025d2a3-d1ed-42cc-b8e4-8687c5e2bc05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>step a-item</value>
      <webElementGuid>14d839ed-b6a2-400b-a74b-50593358796e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                Bimo Satrio Wicaksono
            
            
                Email: bimopnm@gmail.com
            
                
                    Phone: 08123456789
                
                            
                    Fax: 98765
                
                            
                    pnm
                
                            
                    JL. 123
                
                            
                    JL. 321
                
                            
Jakarta                                            ,
                                        12345                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        Bimo Satrio Wicaksono
                    
                    
                        Email: bimopnm@gmail.com
                    
                        
                            Phone: 08123456789
                        
                        
                            Fax: 98765
                        
                        
                            pnm
                        
                        
                            JL. 123
                        
                        
                            JL. 321
                        
                        
Jakarta                                                            ,
                                                        12345                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                1
                        
                        
                            Total:
                            1590.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    1590.00 
                
            
            
                
                    
                        Shipping:
                        
                            (Next Day Air)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            1597.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init('https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/', 'https://demowebshop.tricentis.com/checkout/completed/');
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                </value>
      <webElementGuid>1ddff9cb-26e3-4838-a47e-2be88d1e87f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-step-confirm-order&quot;)</value>
      <webElementGuid>725af570-a695-48cd-9e6c-839b8d21db4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='checkout-step-confirm-order']</value>
      <webElementGuid>9375540b-a673-491e-a61c-7b82a18c0611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='opc-confirm_order']/div[2]</value>
      <webElementGuid>ac49fd31-b77b-42a0-8c19-13d0937b3379</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm order'])[1]/following::div[1]</value>
      <webElementGuid>9131f45e-8b06-434c-a580-0ccd2edd0c76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/following::div[2]</value>
      <webElementGuid>5abcea6c-b420-4315-b430-95ebd1a1ab77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/div[2]</value>
      <webElementGuid>93308515-d177-4def-9ea2-a267cdd1093d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'checkout-step-confirm-order' and (text() = concat(&quot;
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                Bimo Satrio Wicaksono
            
            
                Email: bimopnm@gmail.com
            
                
                    Phone: 08123456789
                
                            
                    Fax: 98765
                
                            
                    pnm
                
                            
                    JL. 123
                
                            
                    JL. 321
                
                            
Jakarta                                            ,
                                        12345                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        Bimo Satrio Wicaksono
                    
                    
                        Email: bimopnm@gmail.com
                    
                        
                            Phone: 08123456789
                        
                        
                            Fax: 98765
                        
                        
                            pnm
                        
                        
                            JL. 123
                        
                        
                            JL. 321
                        
                        
Jakarta                                                            ,
                                                        12345                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                1
                        
                        
                            Total:
                            1590.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    1590.00 
                
            
            
                
                    
                        Shipping:
                        
                            (Next Day Air)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            1597.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init(&quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/completed/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                &quot;) or . = concat(&quot;
                    
    
    
        
        
    
    
    
        

    
        
        
            
                Billing Address
            
            
                Bimo Satrio Wicaksono
            
            
                Email: bimopnm@gmail.com
            
                
                    Phone: 08123456789
                
                            
                    Fax: 98765
                
                            
                    pnm
                
                            
                    JL. 123
                
                            
                    JL. 321
                
                            
Jakarta                                            ,
                                        12345                
                            
                    Indonesia
                
                            
                        Payment Method
                
                
                        Cash On Delivery (COD)
                
        
            
                    
                        
                            Shipping Address
                    
                    
                        Bimo Satrio Wicaksono
                    
                    
                        Email: bimopnm@gmail.com
                    
                        
                            Phone: 08123456789
                        
                        
                            Fax: 98765
                        
                        
                            pnm
                        
                        
                            JL. 123
                        
                        
                            JL. 321
                        
                        
Jakarta                                                            ,
                                                        12345                        
                        
                            Indonesia
                        
                
                    Shipping Method
                
                
                    Next Day Air
                
            
    

        
            
                                                    
                
                
                
                
            
            
                
                                                                
                    
                        Product(s)
                    
                    
                        Price
                    
                    
                        Qty.
                    
                    
                        Total
                    
                
            
            
                    
                                                                            
                                
                            
                        
                            14.1-inch Laptop
                                                                                                            
                        
                            Price:
                            1590.00
                        
                        
                            Qty.:
                                1
                        
                        
                            Total:
                            1590.00
                        
                    
            
        
        
        
        
            
            
            
            
                
    
        
            
                
                    Sub-Total:
                
                
                    1590.00 
                
            
            
                
                    
                        Shipping:
                        
                            (Next Day Air)
                        
                
                
                    
                            0.00
                            
                    
                
            
                
                    
                        Payment method additional fee:
                    
                    
                        7.00
                        
                    
                
                                        
                    
                        Tax: 
                    
                    
                        0.00 
                    
                
                                                
                
                    
                        Total:
                
                
                    
                            1597.00
                    
                
            
        
    


            
        
    


        


                    
                        ConfirmOrder.init(&quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/OpcConfirmOrder/&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;https://demowebshop.tricentis.com/checkout/completed/&quot; , &quot;'&quot; , &quot;);
                    
                    
                        
                            « Back
                        
                        Submitting order information...
                    
                &quot;))]</value>
      <webElementGuid>a9fda94f-2864-4d3a-ad8c-92e7bfcc9a35</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
