<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_shipping_address</name>
   <tag></tag>
   <elementGuidId>e3761460-ef5a-4c88-85f7-31fb6eef7a7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.shipping-info > li.title > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>0c46e65a-5ab9-4994-83e5-5421dad98d2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Shipping Address</value>
      <webElementGuid>1d704ccd-e3e1-4cc7-83b1-091bc5636497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;shipping-info&quot;]/li[@class=&quot;title&quot;]/strong[1]</value>
      <webElementGuid>aa20cbdc-51be-438e-bec1-6fc904215849</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul[2]/li/strong</value>
      <webElementGuid>79c0a54e-afa9-4513-ab6c-1209c4ebbd29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check / Money Order'])[1]/following::strong[1]</value>
      <webElementGuid>b157d404-7078-431c-aec9-2c21a8ee9f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::strong[1]</value>
      <webElementGuid>1ea187b6-40c2-4842-afca-0461d8229314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bimo Satrio Wicaksono'])[2]/preceding::strong[1]</value>
      <webElementGuid>913677ea-ffe2-42f2-aeda-f6454632063e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email: bimopnm@gmail.com'])[2]/preceding::strong[1]</value>
      <webElementGuid>f7b35992-af66-45c2-a91c-273e0ea7dcd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Shipping Address']/parent::*</value>
      <webElementGuid>63b7ae75-fc9d-406c-ba21-527237c5166c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul[2]/li/strong</value>
      <webElementGuid>88788cf0-33aa-4f74-89cd-104bba0a6f72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                            Shipping Address' or . = '
                            Shipping Address')]</value>
      <webElementGuid>ca534c82-46c8-4b30-a22f-fd139a92d3da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
