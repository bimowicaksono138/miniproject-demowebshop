<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_total</name>
   <tag></tag>
   <elementGuidId>afa3c1eb-b7f6-409e-8f44-b5f766c81ecd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/form/div[2]/div[2]/div/table/tbody/tr[5]/td/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4c71a847-2127-450f-abb6-9e880ad56881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nobr</value>
      <webElementGuid>64d2fd07-a454-4018-9369-bec2fad5081e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Total:</value>
      <webElementGuid>7bb38a20-3669-4695-8bae-8364ba735896</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;totals&quot;]/div[@class=&quot;total-info&quot;]/table[@class=&quot;cart-total&quot;]/tbody[1]/tr[5]/td[@class=&quot;cart-total-left&quot;]/span[@class=&quot;nobr&quot;]</value>
      <webElementGuid>5df6dcc8-caa4-44de-be63-902319bcb557</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/form/div[2]/div[2]/div/table/tbody/tr[5]/td/span</value>
      <webElementGuid>327ed725-aa3c-40de-b761-f3ddcb0adce6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tax:'])[1]/following::span[3]</value>
      <webElementGuid>00a34426-b208-45cc-82b9-67ad3ca45826</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment method additional fee:'])[1]/following::span[6]</value>
      <webElementGuid>d0efeb72-ad77-4603-b312-ce2317eafd94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[5]/preceding::span[3]</value>
      <webElementGuid>7a7cc3fe-dd89-4505-becc-2fcfd96bb1bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submitting order information...'])[1]/preceding::span[3]</value>
      <webElementGuid>c15a6710-67a4-4d44-8d22-7d2cbceaca0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[5]/td/span</value>
      <webElementGuid>5f542fc4-664a-4043-a714-9646915f955b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                        Total:' or . = '
                        Total:')]</value>
      <webElementGuid>3eff241f-5b56-4b34-8178-0f3d562a4d71</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
