<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_payment_information</name>
   <tag></tag>
   <elementGuidId>3dd410f8-4d03-4992-a816-bb42cda02072</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.info</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8b5fc0f4-4e7c-4c77-ae69-93010af95568</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>info</value>
      <webElementGuid>57b52c0f-0ce4-4976-bfbe-779a3ad4b314</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            

    
        
            You will pay by COD
        
    


        </value>
      <webElementGuid>210bebe0-1509-47eb-b028-f74aceb5aef5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]</value>
      <webElementGuid>dd6f6257-f5e3-466a-944b-b3f3cabe4e70</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div</value>
      <webElementGuid>f78a8725-0239-4e0e-a2a8-fe394e5ad423</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::div[5]</value>
      <webElementGuid>eb1b2aea-1b3a-4a21-8dc2-c2fd97994344</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::div[6]</value>
      <webElementGuid>d30d8341-ccc1-4896-8b9f-41efe51d260a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[4]/preceding::div[3]</value>
      <webElementGuid>b07b8737-9363-4168-83de-ce3304cee5a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/preceding::div[3]</value>
      <webElementGuid>dab4aa2d-f1f0-4941-ab1a-73c8f4041d52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/form/div/div/div/div</value>
      <webElementGuid>bad9de8b-967a-4d22-90fe-6d199b0d9e42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            

    
        
            You will pay by COD
        
    


        ' or . = '
            

    
        
            You will pay by COD
        
    


        ')]</value>
      <webElementGuid>c1f4f76d-b8fd-4414-ad7d-73acf3b5384b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
