<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radio_shipping_method_2nd_day_air</name>
   <tag></tag>
   <elementGuidId>3158a5c7-1ddd-4c8e-b727-5bb1fe5d3737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='shippingoption_2']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#shippingoption_2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>43f37de6-fc13-437d-a97f-313ecf16c682</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shippingoption_2</value>
      <webElementGuid>1b237695-f5ca-45ca-b22c-a64d9ddc8c92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>0c940ecf-afc4-4a09-9127-73288fca34ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shippingoption</value>
      <webElementGuid>57c96854-c60f-4973-9aaf-9cf1d2c4ab31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>2nd Day Air___Shipping.FixedRate</value>
      <webElementGuid>cce4f6e2-d1b4-47bc-99a6-9320b94abe7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shippingoption_2&quot;)</value>
      <webElementGuid>50a28899-b085-46f9-94cf-a6314d2a6fc3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='shippingoption_2']</value>
      <webElementGuid>ad7fc0c6-3856-4f43-b62b-07a4553c9ee4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-shipping-method-load']/div/div/ul/li[3]/div/input</value>
      <webElementGuid>f0efa3e9-3951-4796-9b03-00ce1f5162bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/input</value>
      <webElementGuid>5c45c039-4106-4ed2-abf6-b2f39e042eb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'shippingoption_2' and @type = 'radio' and @name = 'shippingoption']</value>
      <webElementGuid>87363d54-fbfe-4ee3-943e-0818174b2851</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
