<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_city</name>
   <tag></tag>
   <elementGuidId>e17ac6e0-a223-45b6-ae22-c34319d80942</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='BillingNewAddress_City']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BillingNewAddress_City</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d73ed519-4505-4406-849d-9136e34af3e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>fd2ca236-b712-4f18-9788-2d428e8e400b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4fbcd3ae-e177-4335-bff0-45b0c96ffc29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>City is required</value>
      <webElementGuid>77bc4ec2-2579-4237-b132-45407bb086c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BillingNewAddress_City</value>
      <webElementGuid>7542450d-6bf8-4297-af60-a0056ac5310d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>BillingNewAddress.City</value>
      <webElementGuid>cb5965f5-d226-4459-96fd-7ccb3814787d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>7197e8e8-d85c-4c2e-8141-270327b59bed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BillingNewAddress_City&quot;)</value>
      <webElementGuid>222bc2de-1753-431e-9bfa-e4185c751a22</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='BillingNewAddress_City']</value>
      <webElementGuid>b75a43e0-3264-4ae6-bed0-14b7c166f0f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-new-address-form']/div/div/div/div[7]/input</value>
      <webElementGuid>942ea29b-0998-488a-a3b9-d4a616238cf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/input</value>
      <webElementGuid>a090e1fd-5624-40aa-877c-1643f04642f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'BillingNewAddress_City' and @name = 'BillingNewAddress.City' and @type = 'text']</value>
      <webElementGuid>d384d0ac-3811-43a1-b21f-0abef3e8c4dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
