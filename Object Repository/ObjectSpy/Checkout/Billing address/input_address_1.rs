<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_address_1</name>
   <tag></tag>
   <elementGuidId>77620698-b9f6-4313-b568-cea599d5b954</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='BillingNewAddress_Address1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BillingNewAddress_Address1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ce462544-5c36-4d23-bb05-093ce095dbe0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>21755f0b-c64b-4de5-8b9b-02e658b57c3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>15def400-6847-41a2-897d-4905a6bdf1d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Street address is required</value>
      <webElementGuid>3fd6193b-f78c-401d-95f1-c74d5ff5954b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BillingNewAddress_Address1</value>
      <webElementGuid>a47da1a7-ca68-4347-b34b-6715fd350f05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>BillingNewAddress.Address1</value>
      <webElementGuid>f0ed9a0b-14d6-41e1-b9b8-2192da47a96f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9848543a-b438-43b1-81a0-b4133ecb1f3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BillingNewAddress_Address1&quot;)</value>
      <webElementGuid>7d5cebe7-c0f0-476f-a5fd-631c540b924e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='BillingNewAddress_Address1']</value>
      <webElementGuid>61a30a6c-1c93-4188-86ee-d2d0df63ae72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-new-address-form']/div/div/div/div[8]/input</value>
      <webElementGuid>8d98bd7e-415e-47f7-895f-c88fd076159e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/input</value>
      <webElementGuid>71d0cba2-b3c4-46ef-821f-b38c2fb921de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'BillingNewAddress_Address1' and @name = 'BillingNewAddress.Address1' and @type = 'text']</value>
      <webElementGuid>3da75ad8-e8dc-426c-a6c8-cbaa980cf41d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
