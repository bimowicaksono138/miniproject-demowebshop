<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_fax_number</name>
   <tag></tag>
   <elementGuidId>9c873699-ddc3-41ea-8b41-54c3b188c105</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='BillingNewAddress_FaxNumber']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#BillingNewAddress_FaxNumber</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>41ee7563-1f63-4afd-ae98-b4d00544f549</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>6add8ec6-0f6e-450c-9df3-8cc214263785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>BillingNewAddress_FaxNumber</value>
      <webElementGuid>bf470c6a-44cc-4230-aaaf-06c0a5e9fe08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>BillingNewAddress.FaxNumber</value>
      <webElementGuid>11341ebc-74a2-4d85-b3ee-6e251943c521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>78640025-5738-4f01-856e-fbb84f2edf88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;BillingNewAddress_FaxNumber&quot;)</value>
      <webElementGuid>2c79d6ed-a093-499b-b53b-01b7d0c1dfd8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='BillingNewAddress_FaxNumber']</value>
      <webElementGuid>24db66f1-7dec-4443-9eae-e2442e9bfd24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='billing-new-address-form']/div/div/div/div[12]/input</value>
      <webElementGuid>a6304738-df0c-4ea7-9879-9a6e4c91a247</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/input</value>
      <webElementGuid>4494626c-1c2c-4b7a-8fe8-a757c4473ea4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'BillingNewAddress_FaxNumber' and @name = 'BillingNewAddress.FaxNumber' and @type = 'text']</value>
      <webElementGuid>fcd761f6-c510-4e96-871c-8964846ddc29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
