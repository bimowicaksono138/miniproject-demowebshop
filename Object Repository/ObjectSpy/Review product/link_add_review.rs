<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_add_review</name>
   <tag></tag>
   <elementGuidId>88310945-f647-46d7-b2a7-66ec7fc71f28</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='product-details-form']/div/div/div[2]/div[4]/div[2]/a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>59b3d547-1489-426a-89af-ac3093c68013</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/productreviews/31</value>
      <webElementGuid>20a3d797-a8d5-4aca-92ef-e3b6a96d4f52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add your review</value>
      <webElementGuid>7453cf21-267e-49d6-8d5d-212cb99b00d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product-details-form&quot;)/div[1]/div[@class=&quot;product-essential&quot;]/div[@class=&quot;overview&quot;]/div[@class=&quot;product-reviews-overview&quot;]/div[@class=&quot;product-review-links&quot;]/a[2]</value>
      <webElementGuid>234ab7fc-d45b-4fc8-bfed-9ee45f9e506f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='product-details-form']/div/div/div[2]/div[4]/div[2]/a[2]</value>
      <webElementGuid>04e967cd-6383-4bae-ac75-a868c3957423</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Add your review')]</value>
      <webElementGuid>e9b65b4f-3213-45cb-9d42-298d9ae57809</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[1]/following::a[1]</value>
      <webElementGuid>b359787f-5ce9-4a41-8cab-617da8c6be87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In stock'])[1]/following::a[2]</value>
      <webElementGuid>66c71907-be5c-4500-9605-c0e04c6f00bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty:'])[1]/preceding::a[1]</value>
      <webElementGuid>47330608-fda3-47d7-8ce5-d72bf2fed6d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Products specifications'])[1]/preceding::a[7]</value>
      <webElementGuid>d405369a-c19e-44fc-b82c-21e16c458f7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add your review']/parent::*</value>
      <webElementGuid>8b50b195-db2f-4acf-b66d-d33e314bddf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/productreviews/31')])[2]</value>
      <webElementGuid>85b93c0c-c1ec-418a-98a7-4c767fbfe586</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>15718a0c-f641-4c9e-ae0c-ee6be118d864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/productreviews/31' and (text() = 'Add your review' or . = 'Add your review')]</value>
      <webElementGuid>c1151241-e4c0-408d-bee3-ea333b5d52d7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
