<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_display_perpage</name>
   <tag></tag>
   <elementGuidId>5457532a-5932-4c89-96e4-bd3e3706f561</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='products-pagesize']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#products-pagesize</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>8b08943b-065f-4036-a50a-70f5fb3563ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>products-pagesize</value>
      <webElementGuid>b3d831a3-bff8-47fd-b8f6-f6100296fb31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>products-pagesize</value>
      <webElementGuid>6d3b0f67-a538-4663-8e0f-72886f615e0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>setLocation(this.value);</value>
      <webElementGuid>9986b83e-c4ad-4d25-a9e0-fdafa461d2f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>4
8
12
</value>
      <webElementGuid>d298b394-35c4-4720-9761-51217f026876</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products-pagesize&quot;)</value>
      <webElementGuid>0fdc3048-f078-49b4-8421-ec5baff0b113</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='products-pagesize']</value>
      <webElementGuid>f23145a6-ba22-4a59-aa8b-ba46bdf8c5dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::select[1]</value>
      <webElementGuid>8c3c9ff9-2f31-4f0a-9c27-06cd23f6361e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::select[2]</value>
      <webElementGuid>5a09e20f-5894-4046-8842-3d5fa98424d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/preceding::select[1]</value>
      <webElementGuid>8095b87a-b283-42fb-bc42-1032a3a416be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Build your own cheap computer'])[1]/preceding::select[1]</value>
      <webElementGuid>2046bc94-03c5-4a05-abfb-df66f5617cc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[3]/select</value>
      <webElementGuid>fd3e8412-1747-4fb3-9efb-69cd3957b554</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'products-pagesize' and @name = 'products-pagesize' and (text() = '4
8
12
' or . = '4
8
12
')]</value>
      <webElementGuid>98d0b202-fce1-4aca-9904-91e25ac9afee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
