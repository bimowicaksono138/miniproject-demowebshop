<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_sub_category</name>
   <tag></tag>
   <elementGuidId>d26e01f9-b21e-4e9a-8b0b-3d9ac76a9f0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Isc']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Isc</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a38823f2-43ac-40df-b9f1-c48d83b365ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Isc</value>
      <webElementGuid>4b5f4bcb-e2e2-46f5-b938-39a860d9e22f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Isc</value>
      <webElementGuid>6a77f921-9cea-47b5-8ce4-7558f0656fa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>48d3913f-a5a4-4c3b-81b3-35eb1eb4eead</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a2590571-ac4a-4428-a138-06cd21db377d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Isc&quot;)</value>
      <webElementGuid>c18eee8b-1474-459d-af23-a6b8bd859127</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Isc']</value>
      <webElementGuid>f5323fca-648f-4941-a678-c6669739b3bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div[2]/input</value>
      <webElementGuid>ce96018e-a975-4f7c-8774-3dd95f7e3e9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>f095ce0a-ba7d-4e8b-bf11-c1dbff599039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Isc' and @name = 'Isc' and @type = 'checkbox']</value>
      <webElementGuid>a08ceaa3-3fb0-4e19-9764-e41b8eb58db3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
