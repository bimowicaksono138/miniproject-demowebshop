<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_sortby</name>
   <tag></tag>
   <elementGuidId>ac1398f4-8b62-4449-863c-64859a7ba4dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='products-orderby']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#products-orderby</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>04f744bf-3462-4cfa-ab09-c3917f76b196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>products-orderby</value>
      <webElementGuid>55549637-6f36-4f51-9c50-671a6148f7ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>products-orderby</value>
      <webElementGuid>77d2cbf0-1cfd-4967-bb20-eeb3fe6c13b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>setLocation(this.value);</value>
      <webElementGuid>d99f642d-9ffb-4c3e-8708-aacff7e6ce1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
</value>
      <webElementGuid>b42cf577-ab36-419b-8207-bcc2090d502c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products-orderby&quot;)</value>
      <webElementGuid>1bd1a2d6-e748-4829-8774-0d4abb5a4ca9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='products-orderby']</value>
      <webElementGuid>fbe827f0-3768-48a6-a045-749b629124ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sort by'])[1]/following::select[1]</value>
      <webElementGuid>7dd2daab-3cdd-49a5-9259-23a22e3a7620</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View as'])[1]/following::select[2]</value>
      <webElementGuid>3e586377-b56a-46e9-a887-49ed412c4018</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/preceding::select[1]</value>
      <webElementGuid>98ca4080-29af-4858-b0bd-5b4a47e35e7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/preceding::select[2]</value>
      <webElementGuid>d285d99d-ea47-4765-804b-3811f8c26d52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>00b91a02-02e1-4d7e-9bb3-b0af06d5fa83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'products-orderby' and @name = 'products-orderby' and (text() = 'Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
' or . = 'Position
Name: A to Z
Name: Z to A
Price: Low to High
Price: High to Low
Created on
')]</value>
      <webElementGuid>d5e9e837-3e9b-4ba4-89e7-6d388a31bad3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
