<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_first_name</name>
   <tag></tag>
   <elementGuidId>a09f259e-3a1f-43b0-a20c-c7a51e62e5b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='FirstName']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#FirstName</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>da5d7514-5deb-4bd2-857b-7b21c9d9e0ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>98f8b0dd-84d0-45c0-9d6a-25d92eac4811</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7e716ea5-98d6-4b94-9d38-5676ea1a0ba4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>First name is required.</value>
      <webElementGuid>312d8d72-84ad-4fe6-a6dc-5276e7a07ab0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>8b079af4-f04b-4d04-b3da-bb6fbb58f156</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>c699a295-4943-446e-9a0c-0c6e0c26b78b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>19e8e0dc-2b5b-4a4c-9446-6b412d0261cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FirstName&quot;)</value>
      <webElementGuid>ddd1cceb-d521-4fce-b1de-b63107f5aab2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='FirstName']</value>
      <webElementGuid>12b65fb0-cc92-42e0-b790-77ea322a778c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>aeca747e-686a-4c4d-b80e-9f41a78b5d51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'FirstName' and @name = 'FirstName' and @type = 'text']</value>
      <webElementGuid>34d93385-d7fa-466a-89c4-df078fe4697c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
