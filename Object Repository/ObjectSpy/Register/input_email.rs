<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_email</name>
   <tag></tag>
   <elementGuidId>c2e1c127-951a-4361-a5bc-009fc97dacff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Email']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Email</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7de9b7c0-32d9-4be3-9b46-44a539a0fbc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>2a8df2cb-1e2e-4342-854b-586a56a7440d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ebc339b4-647b-4144-a793-58bc1cd335ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-email</name>
      <type>Main</type>
      <value>Wrong email</value>
      <webElementGuid>a39dee62-7122-47d6-98ce-dbc43750a35e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>Email is required.</value>
      <webElementGuid>d79e8118-7dea-4127-8dce-4e7f18dab29b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>bce5423b-ace1-427b-bd50-587e29ec28b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>01e3edcf-99f8-4fa7-bc67-7f6af24ebf44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>fd4112ed-03a5-4013-a5cf-4320ebca20df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Email&quot;)</value>
      <webElementGuid>e76ed44a-be36-4a4e-8116-af4a1996cb56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Email']</value>
      <webElementGuid>5a9ee319-e395-4493-a424-8ba7fd33b2d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/input</value>
      <webElementGuid>5e16c815-9c50-4d4f-9fc6-fe12bfd27aa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Email' and @name = 'Email' and @type = 'text']</value>
      <webElementGuid>a974cb00-8e41-48bb-9f98-12d06fbc6447</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
