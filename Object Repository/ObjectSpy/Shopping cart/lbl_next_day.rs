<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_next_day</name>
   <tag></tag>
   <elementGuidId>e64a63f0-849f-465f-bcf8-2b5ea5327b20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Ground (0.00)'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>bc51cd05-7a88-4969-bd92-76df757d4b0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>option-name</value>
      <webElementGuid>dd481953-97ef-42f0-8a0a-a7f9bff2482b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Next Day Air (0.00)
                            </value>
      <webElementGuid>f129df39-f9a4-4878-9621-0585537be2ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;shipping&quot;]/div[@class=&quot;estimate-shipping&quot;]/ul[@class=&quot;shipping-results&quot;]/li[@class=&quot;shipping-option-item&quot;]/strong[@class=&quot;option-name&quot;]</value>
      <webElementGuid>5b495b53-cc11-4440-892a-9486e8d89e4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ground (0.00)'])[1]/following::strong[1]</value>
      <webElementGuid>446a6bcb-568d-40fe-9107-8af3f704b863</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip / postal code:'])[1]/following::strong[2]</value>
      <webElementGuid>22d8bd2c-ba01-477c-92ca-bb015faeb6b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The one day air shipping'])[1]/preceding::strong[1]</value>
      <webElementGuid>1c737b1e-a784-48c9-8d56-76ada6749d7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The two day air shipping'])[1]/preceding::strong[2]</value>
      <webElementGuid>b53fbc10-df27-444f-bb72-c441ce9ec3c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next Day Air (0.00)']/parent::*</value>
      <webElementGuid>038b4538-43e1-4587-870d-5ba5e0bba899</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/strong</value>
      <webElementGuid>cf6d759f-9a01-470b-8adf-23bb2d0668a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                                Next Day Air (0.00)
                            ' or . = '
                                Next Day Air (0.00)
                            ')]</value>
      <webElementGuid>b3593c66-b553-46ac-b7b7-7dcf91aa228e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
