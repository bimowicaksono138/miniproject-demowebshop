<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_in_store_pickup</name>
   <tag></tag>
   <elementGuidId>2d847c6d-a299-4a84-99f4-44d9df742549</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='The two day air shipping'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>4d998582-c93a-48ef-acba-4fd9b1e7d023</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>option-name</value>
      <webElementGuid>f2621652-c41b-4431-b03f-d75287e6e5d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                In-Store Pickup (0.00)
                            </value>
      <webElementGuid>1f00a256-85ac-45a4-8e20-3ed0a99bd33a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;shipping&quot;]/div[@class=&quot;estimate-shipping&quot;]/ul[@class=&quot;shipping-results&quot;]/li[@class=&quot;shipping-option-item&quot;]/strong[@class=&quot;option-name&quot;]</value>
      <webElementGuid>235766a9-7c44-4605-acda-3e8782399553</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The two day air shipping'])[1]/following::strong[1]</value>
      <webElementGuid>f329274e-9769-44bf-a3bf-51ca28b204b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The one day air shipping'])[1]/following::strong[2]</value>
      <webElementGuid>b2b37903-78f1-4c83-8a58-64f6eec3d063</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pick up your items at the store (put your store address here)'])[1]/preceding::strong[1]</value>
      <webElementGuid>506e860c-6a07-4f0b-861f-c6f46e82fc54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sub-Total:'])[1]/preceding::strong[1]</value>
      <webElementGuid>7af7e452-db56-4011-8289-cd5be7fa4019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='In-Store Pickup (0.00)']/parent::*</value>
      <webElementGuid>fcdfc700-99f1-4c44-9f84-e6752f9f619f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/strong</value>
      <webElementGuid>9f46a26b-2d1f-460f-8181-3bbbce9755a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                                In-Store Pickup (0.00)
                            ' or . = '
                                In-Store Pickup (0.00)
                            ')]</value>
      <webElementGuid>46cd931e-7f2f-46f8-9d63-26cd2945b48d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
