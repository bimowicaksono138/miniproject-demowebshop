<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_state_province</name>
   <tag></tag>
   <elementGuidId>dcba5dd6-61b3-40ec-b71c-5e74a0981320</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='StateProvinceId']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#StateProvinceId</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d9e8565b-a687-4056-adfb-ebd744538f6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>state-input</value>
      <webElementGuid>a4107e1a-e70b-48e6-a196-e47967cb9f45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b6dd8ae7-cfdd-44f1-bfd7-c9ca3b2e28f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-number</name>
      <type>Main</type>
      <value>The field State / province must be a number.</value>
      <webElementGuid>29e5d006-2bb0-4df3-bf06-0091974d8dc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>StateProvinceId</value>
      <webElementGuid>93ed5ba7-11d7-4aa2-b5e6-a17517f3eb60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>StateProvinceId</value>
      <webElementGuid>563abff6-e720-433e-90a5-eed13d26469a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Other (Non US)
</value>
      <webElementGuid>7fcb27b6-6066-4da0-8088-344c42240a9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;StateProvinceId&quot;)</value>
      <webElementGuid>48ca9e1f-e9d9-48aa-9dd9-11735949baa1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='StateProvinceId']</value>
      <webElementGuid>100013a2-6afa-4fff-96d9-6638a2c031a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='State / province:'])[1]/following::select[1]</value>
      <webElementGuid>27723eb3-bee9-460b-88bd-2a794e524157</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::select[1]</value>
      <webElementGuid>3809088b-332e-48ac-a8cd-e319c3fe2086</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/preceding::select[1]</value>
      <webElementGuid>fd33b028-62e4-498f-ba2b-01121d1bdea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zip / postal code:'])[1]/preceding::select[1]</value>
      <webElementGuid>a8e340e7-6b2b-4405-aec8-aaddea620b1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>1ea567e2-86e4-4bd8-8d28-3972c409fb7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'StateProvinceId' and @name = 'StateProvinceId' and (text() = 'Other (Non US)
' or . = 'Other (Non US)
')]</value>
      <webElementGuid>b62832a1-046f-4ae8-a51f-eb3ae513c61f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
