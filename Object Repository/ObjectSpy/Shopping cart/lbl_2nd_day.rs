<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_2nd_day</name>
   <tag></tag>
   <elementGuidId>7a492ba0-0d20-44fa-8b23-ecb780ac0a36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='The one day air shipping'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>30c8361e-a9e6-46bb-8f57-d69629fff890</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>option-name</value>
      <webElementGuid>7c87a339-97c4-4517-8502-7f45322f5071</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                2nd Day Air (0.00)
                            </value>
      <webElementGuid>c2fe7d49-029b-4ff5-a79e-3937c11df5a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;shipping&quot;]/div[@class=&quot;estimate-shipping&quot;]/ul[@class=&quot;shipping-results&quot;]/li[@class=&quot;shipping-option-item&quot;]/strong[@class=&quot;option-name&quot;]</value>
      <webElementGuid>244babe6-26b9-4812-924b-2cdc58fe6176</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The one day air shipping'])[1]/following::strong[1]</value>
      <webElementGuid>2c8dc7d8-362b-4c89-99af-54b501e6fad2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next Day Air (0.00)'])[1]/following::strong[1]</value>
      <webElementGuid>440dd2dd-c826-413e-8873-d8f929bda93b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The two day air shipping'])[1]/preceding::strong[1]</value>
      <webElementGuid>bab4fa14-a115-481f-bced-06d3da71423d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In-Store Pickup (0.00)'])[1]/preceding::strong[1]</value>
      <webElementGuid>09da13ff-deae-4488-9b8a-d0fe878898aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='2nd Day Air (0.00)']/parent::*</value>
      <webElementGuid>e2234715-ae95-42b7-82c7-37e8f5b15f74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/strong</value>
      <webElementGuid>4b4af7ed-e80c-435f-a7f3-47662b7e877f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                                2nd Day Air (0.00)
                            ' or . = '
                                2nd Day Air (0.00)
                            ')]</value>
      <webElementGuid>218421db-734d-4270-873a-5a6305f8e64a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
