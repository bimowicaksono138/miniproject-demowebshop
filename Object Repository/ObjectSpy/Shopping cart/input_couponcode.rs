<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_couponcode</name>
   <tag></tag>
   <elementGuidId>a3290ba7-ffdc-4895-b918-3bf555336ca8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='discountcouponcode']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;discountcouponcode&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8e9c1261-aa2f-4b10-9028-1dd80d7ed621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>discountcouponcode</value>
      <webElementGuid>480b3d89-5b3b-49bf-ad33-0f8f2ab1e4c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>b414b66d-40e5-47df-852a-0db52fc04b2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>discount-coupon-code</value>
      <webElementGuid>fc60b6d0-ed38-465e-8be8-b883caf4cd12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page shopping-cart-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;order-summary-content&quot;]/form[1]/div[@class=&quot;cart-footer&quot;]/div[@class=&quot;cart-collaterals&quot;]/div[@class=&quot;deals&quot;]/div[@class=&quot;coupon-box&quot;]/div[@class=&quot;coupon-code&quot;]/input[@class=&quot;discount-coupon-code&quot;]</value>
      <webElementGuid>b3ed086d-4bc8-4697-814c-6d5c0014edbc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='discountcouponcode']</value>
      <webElementGuid>6edbfbcd-bf5d-434b-b959-f4f850070582</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/input</value>
      <webElementGuid>e6f35d77-ecd1-400a-97d3-6b4cf7e1ccda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'discountcouponcode' and @type = 'text']</value>
      <webElementGuid>b59fc910-d646-42c3-b9f3-4dcadaf35985</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
