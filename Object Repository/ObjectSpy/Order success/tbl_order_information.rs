<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tbl_order_information</name>
   <tag></tag>
   <elementGuidId>9210198d-71d2-4aaa-afdd-3ce544ac4103</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='PDF Invoice'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.page-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0e14879e-9700-4fe5-b9b4-0fa2958d2c57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-body</value>
      <webElementGuid>961eb5bf-d6c2-49e8-9330-2eba4bf69862</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        
            
                Order #1620586
            
            
                Order Date: Thursday, February 29, 2024
                Order Status: Pending
            
            
                Order Total: 23857.00
            
            
        
        
            
                
                    
                        
                            
                                
                                    
                                        Billing Address
                                
                                
                                    Bimo Satrio Wicaksono
                                
                                
                                    Email: bimopnm@gmail.com
                                
                                    
                                        Phone: 08123456789
                                    
                                                                    
                                        Fax: 98765
                                    
                                                                    
                                        pnm
                                    
                                                                    
                                        JL. 123
                                    
                                                                    
                                        JL. 321
                                    
                                                                    
Jakarta                                                                                    ,
                                                                                12345                                    
                                                                    
                                        Indonesia
                                    
                                                                                                    
                                        Payment Method
                                    
                                    
                                        Cash On Delivery (COD)
                                    
                                                            
                        
                            
                                
                                        
                                            
                                                Shipping Address
                                        
                                        
                                            Bimo Satrio Wicaksono
                                        
                                        
                                            Email: bimopnm@gmail.com
                                        
                                            
                                                Phone: 08123456789
                                            
                                            
                                                Fax: 98765
                                            
                                            
                                                pnm
                                            
                                            
                                                JL. 123
                                            
                                            
                                                JL. 321
                                            
                                            
Jakarta                                                                                                    ,
                                                                                                12345                                            
                                            
                                                Indonesia
                                            
                                    
                                        Shipping Method
                                    
                                    
                                        Ground
                                    
                                
                            
                    
                
            
        
        
            
                
                    Product(s)
                
                
                    
                        
                        
                        
                        
                    
                    
                        
                            
                                Name
                            
                            
                                Price
                            
                            
                                Quantity
                            
                            
                                Total
                            
                        
                    
                    
                            
                                
                                        14.1-inch Laptop
                                                                    
                                
                                    Price:
                                    1590.00
                                
                                
                                    Quantity:
                                    15
                                
                                
                                    Total:
                                    23850.00
                                
                            
                    
                
                                    
                            
                                            
            
        
        
            
                
                    
                        
                            
                                Sub-Total:
                            
                        
                        
                            
                                23850.00
                            
                        
                    
                                            
                            
                                
                                    Shipping:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                            
                            
                                
                                    Payment method additional fee:
                                
                            
                            
                                
                                    7.00
                                
                            
                        
                                                                
                            
                                
                                    Tax:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                                                                
                        
                            
                                Order Total:
                            
                        
                        
                            
                                23857.00
                            
                        
                    
                
            
        
                
    </value>
      <webElementGuid>a376bdb8-b289-464a-8eeb-6eefd698acb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page order-details-page&quot;]/div[@class=&quot;page-body&quot;]</value>
      <webElementGuid>e771a2f2-7d63-454f-bdcd-774ce91218ba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDF Invoice'])[1]/following::div[1]</value>
      <webElementGuid>335d1b45-2d3c-4c07-8306-ac19856fbbd7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print'])[1]/following::div[1]</value>
      <webElementGuid>22f6173c-d30a-4be0-99df-da36d82e273d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]</value>
      <webElementGuid>9a6e5326-863a-4ac2-b950-e3a812c03d5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        
        
            
                Order #1620586
            
            
                Order Date: Thursday, February 29, 2024
                Order Status: Pending
            
            
                Order Total: 23857.00
            
            
        
        
            
                
                    
                        
                            
                                
                                    
                                        Billing Address
                                
                                
                                    Bimo Satrio Wicaksono
                                
                                
                                    Email: bimopnm@gmail.com
                                
                                    
                                        Phone: 08123456789
                                    
                                                                    
                                        Fax: 98765
                                    
                                                                    
                                        pnm
                                    
                                                                    
                                        JL. 123
                                    
                                                                    
                                        JL. 321
                                    
                                                                    
Jakarta                                                                                    ,
                                                                                12345                                    
                                                                    
                                        Indonesia
                                    
                                                                                                    
                                        Payment Method
                                    
                                    
                                        Cash On Delivery (COD)
                                    
                                                            
                        
                            
                                
                                        
                                            
                                                Shipping Address
                                        
                                        
                                            Bimo Satrio Wicaksono
                                        
                                        
                                            Email: bimopnm@gmail.com
                                        
                                            
                                                Phone: 08123456789
                                            
                                            
                                                Fax: 98765
                                            
                                            
                                                pnm
                                            
                                            
                                                JL. 123
                                            
                                            
                                                JL. 321
                                            
                                            
Jakarta                                                                                                    ,
                                                                                                12345                                            
                                            
                                                Indonesia
                                            
                                    
                                        Shipping Method
                                    
                                    
                                        Ground
                                    
                                
                            
                    
                
            
        
        
            
                
                    Product(s)
                
                
                    
                        
                        
                        
                        
                    
                    
                        
                            
                                Name
                            
                            
                                Price
                            
                            
                                Quantity
                            
                            
                                Total
                            
                        
                    
                    
                            
                                
                                        14.1-inch Laptop
                                                                    
                                
                                    Price:
                                    1590.00
                                
                                
                                    Quantity:
                                    15
                                
                                
                                    Total:
                                    23850.00
                                
                            
                    
                
                                    
                            
                                            
            
        
        
            
                
                    
                        
                            
                                Sub-Total:
                            
                        
                        
                            
                                23850.00
                            
                        
                    
                                            
                            
                                
                                    Shipping:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                            
                            
                                
                                    Payment method additional fee:
                                
                            
                            
                                
                                    7.00
                                
                            
                        
                                                                
                            
                                
                                    Tax:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                                                                
                        
                            
                                Order Total:
                            
                        
                        
                            
                                23857.00
                            
                        
                    
                
            
        
                
    ' or . = '
        
        
            
                Order #1620586
            
            
                Order Date: Thursday, February 29, 2024
                Order Status: Pending
            
            
                Order Total: 23857.00
            
            
        
        
            
                
                    
                        
                            
                                
                                    
                                        Billing Address
                                
                                
                                    Bimo Satrio Wicaksono
                                
                                
                                    Email: bimopnm@gmail.com
                                
                                    
                                        Phone: 08123456789
                                    
                                                                    
                                        Fax: 98765
                                    
                                                                    
                                        pnm
                                    
                                                                    
                                        JL. 123
                                    
                                                                    
                                        JL. 321
                                    
                                                                    
Jakarta                                                                                    ,
                                                                                12345                                    
                                                                    
                                        Indonesia
                                    
                                                                                                    
                                        Payment Method
                                    
                                    
                                        Cash On Delivery (COD)
                                    
                                                            
                        
                            
                                
                                        
                                            
                                                Shipping Address
                                        
                                        
                                            Bimo Satrio Wicaksono
                                        
                                        
                                            Email: bimopnm@gmail.com
                                        
                                            
                                                Phone: 08123456789
                                            
                                            
                                                Fax: 98765
                                            
                                            
                                                pnm
                                            
                                            
                                                JL. 123
                                            
                                            
                                                JL. 321
                                            
                                            
Jakarta                                                                                                    ,
                                                                                                12345                                            
                                            
                                                Indonesia
                                            
                                    
                                        Shipping Method
                                    
                                    
                                        Ground
                                    
                                
                            
                    
                
            
        
        
            
                
                    Product(s)
                
                
                    
                        
                        
                        
                        
                    
                    
                        
                            
                                Name
                            
                            
                                Price
                            
                            
                                Quantity
                            
                            
                                Total
                            
                        
                    
                    
                            
                                
                                        14.1-inch Laptop
                                                                    
                                
                                    Price:
                                    1590.00
                                
                                
                                    Quantity:
                                    15
                                
                                
                                    Total:
                                    23850.00
                                
                            
                    
                
                                    
                            
                                            
            
        
        
            
                
                    
                        
                            
                                Sub-Total:
                            
                        
                        
                            
                                23850.00
                            
                        
                    
                                            
                            
                                
                                    Shipping:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                            
                            
                                
                                    Payment method additional fee:
                                
                            
                            
                                
                                    7.00
                                
                            
                        
                                                                
                            
                                
                                    Tax:
                                
                            
                            
                                
                                    0.00
                                
                            
                        
                                                                                
                        
                            
                                Order Total:
                            
                        
                        
                            
                                23857.00
                            
                        
                    
                
            
        
                
    ')]</value>
      <webElementGuid>3da34e23-b91a-4e04-b79b-1a7f81f61513</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
