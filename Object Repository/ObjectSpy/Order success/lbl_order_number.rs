<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_order_number</name>
   <tag></tag>
   <elementGuidId>87ef419a-c5ac-4aab-b607-942e9aa1a45a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Your order has been successfully processed!'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.details > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>bc8d1f13-2dd3-4ba9-b7f7-e0e2df6ca4f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Order number: 1620586
                </value>
      <webElementGuid>fb5ca12e-25da-4d5f-aae6-f304e5c3a630</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page checkout-page&quot;]/div[@class=&quot;page-body checkout-data&quot;]/div[@class=&quot;section order-completed&quot;]/ul[@class=&quot;details&quot;]/li[1]</value>
      <webElementGuid>983d73cb-67ed-4383-8156-b1b73e12dc7d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your order has been successfully processed!'])[1]/following::li[1]</value>
      <webElementGuid>617bdbda-bbd3-40a8-8dab-c06029b19e41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you'])[1]/following::li[1]</value>
      <webElementGuid>211cb32f-c4cf-47ee-804d-fa0b9fb17ad9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Click here for order details.'])[1]/preceding::li[1]</value>
      <webElementGuid>c24a6c9a-981f-4f28-96ff-bffdfd561ecc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::li[2]</value>
      <webElementGuid>eb008e45-2f6d-456a-8083-fbd3ed496c86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Order number: 1620586']/parent::*</value>
      <webElementGuid>500d7cc5-0b3a-4662-a76e-61ff1b2560d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[4]/div/div/div[2]/div/ul/li</value>
      <webElementGuid>9e388e1d-75c8-4e11-b753-b23911691558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                    Order number: 1620586
                ' or . = '
                    Order number: 1620586
                ')]</value>
      <webElementGuid>39cc0e20-2b08-4c70-a751-9c7012ce93b8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
