<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_pdf_invoice</name>
   <tag></tag>
   <elementGuidId>b8c33a73-dcca-4452-84ec-541ea3a4b561</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'PDF Invoice')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.button-2.pdf-order-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9bbfd11a-2a0a-4293-9275-2fed6669bc85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/orderdetails/pdf/1620586</value>
      <webElementGuid>dacb2f36-29ca-40aa-9e75-17f88c36f4a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-2 pdf-order-button</value>
      <webElementGuid>13528932-66e6-43b0-9814-56b607854af9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PDF Invoice</value>
      <webElementGuid>184365e4-0af1-4d06-b59d-16cb36c3ffb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-1&quot;]/div[@class=&quot;page order-details-page&quot;]/div[@class=&quot;page-title&quot;]/a[@class=&quot;button-2 pdf-order-button&quot;]</value>
      <webElementGuid>613d9ba9-5b57-4b96-abfd-f219b085079b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'PDF Invoice')]</value>
      <webElementGuid>bfa41862-4106-48e6-b057-217e111e1689</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Print'])[1]/following::a[1]</value>
      <webElementGuid>30508712-eb4b-4db4-9615-455d614039cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order information'])[1]/following::a[2]</value>
      <webElementGuid>7ed0a2a5-f016-4d27-b33e-26dd628cad20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order #1620586'])[1]/preceding::a[1]</value>
      <webElementGuid>676c607c-54a0-4e77-b9fa-715fdc3e3f39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Date: Thursday, February 29, 2024'])[1]/preceding::a[1]</value>
      <webElementGuid>d7f600be-faa9-4727-9b3a-b3f819aaa8ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PDF Invoice']/parent::*</value>
      <webElementGuid>ed7850e0-a3dd-4c35-b853-5ff848e6024f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/orderdetails/pdf/1620586')]</value>
      <webElementGuid>fd26f013-1fa1-44c4-b92c-24d17fb0861a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>69a2ea8e-3eb6-4431-80a3-1b98c3d195c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/orderdetails/pdf/1620586' and (text() = 'PDF Invoice' or . = 'PDF Invoice')]</value>
      <webElementGuid>6bae6b5e-de02-46df-89be-38d37334246b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
